param(
    [string]$DevopsUrl,
    [string]$PersonalAccessToken,
    [string]$DeploymentPool,
    [int]$DebuggerPort,
    [string]$WindowsLogonPassword,
    [string]$WindowsAccount,
    [string]$ProjectName,
    [string]$DeploymentGroup
)
$TempPath="C:\Temp"
$AgentPath="C:\Agent"
# install remote debugger tools
if(-not (Test-Path $TempPath)){mkdir $TempPath}
wget https://aka.ms/vs/17/release/RemoteTools.amd64ret.deu.exe -OutFile "$TempPath\vs_remotetools.exe"
C:\Temp\vs_remotetools.exe /install /passive
Start-Sleep -Seconds 120
cd "C:\Program Files\Microsoft Visual Studio 17.0\Common7\IDE\Remote Debugger\x64\"
.\msvsmon.exe /allow $WindowsAccount /port $DebuggerPort /nosecuritywarn

If(-NOT (Test-Path $env:SystemDrive\'azagent')){mkdir $env:SystemDrive\'azagent'}; cd $env:SystemDrive\'azagent'; for($i=1; $i -lt 100; $i++){$destFolder="A"+$i.ToString();if(-NOT (Test-Path ($destFolder))){mkdir $destFolder;cd $destFolder;break;}}; $agentZip="$PWD\agent.zip";$DefaultProxy=[System.Net.WebRequest]::DefaultWebProxy;$securityProtocol=@();$securityProtocol+=[Net.ServicePointManager]::SecurityProtocol;$securityProtocol+=[Net.SecurityProtocolType]::Tls12;[Net.ServicePointManager]::SecurityProtocol=$securityProtocol;$WebClient=New-Object Net.WebClient; $Uri='https://vstsagentpackage.azureedge.net/agent/2.195.2/vsts-agent-win-x64-2.195.2.zip';if($DefaultProxy -and (-not $DefaultProxy.IsBypassed($Uri))){$WebClient.Proxy= New-Object Net.WebProxy($DefaultProxy.GetProxy($Uri).OriginalString, $True);}; $WebClient.DownloadFile($Uri, $agentZip);Add-Type -AssemblyName System.IO.Compression.FileSystem;[System.IO.Compression.ZipFile]::ExtractToDirectory( $agentZip, "$PWD");.\config.cmd --unattended --deploymentgroup --deploymentgroupname $DeploymentGroup --agent $env:COMPUTERNAME --runasservice ---runAsAutoLogon -work '_work' --url 'https://dev.azure.com/SD-Daimler/' --projectname $ProjectName --auth PAT --token $PersonalAccessToken --userName $WindowsAccount --password $WindowsLogonPassword --replace --deploymentGroupTags "Windows10"  --windowsLogonAccount $WindowsAccount --windowsLogonPassword $WindowsLogonPassword; Remove-Item $agentZip;
